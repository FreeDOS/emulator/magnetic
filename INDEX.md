# Magnetic

An interpreter for Magnetic Scrolls adventures.


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## MAGNETIC.LSM

<table>
<tr><td>title</td><td>Magnetic</td></tr>
<tr><td>version</td><td>2.3a</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-12-11</td></tr>
<tr><td>description</td><td>An interpreter for Magnetic Scrolls adventures.</td></tr>
<tr><td>author</td><td>Niclas Karlsson</td></tr>
<tr><td>platforms</td><td>DOS, Amiga, MacOS X, Java, Maemo5, OpenPandora, Windows, GNU/Linux</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
</table>
